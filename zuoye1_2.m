%project 1_2 
format long
precision = input('the level of precision:   ') 
m = 10000; 
dif = 1; % set the difference
p = 0; 
pi_hat = ones(m,1); 
n = 100;
while dif >= precision 
    x = rand(n,1); 
    y = rand(n,1); 
    for i = 1:n 
        if x(i)^2 + y(i)^2 <= 1 
            p = p + 1; % count points if they're in the quarter of circle
        end 
    end 
    pi_hat(n) = 4*p/n; 
    dif = abs(pi_hat(n) - pi_hat(n - 50)); % compute the difference 
    n = n + 50; 
end
pi(n) = pi_hat(n - 50) 
step = (n - 50)/50 + 1 % count steps
