% project 1_3
function output = pi(e) % define a function
m = 100000;
pi_hat = ones(m,1); % these parts are the same as question 2
dif = 1;
n = 1000;
while dif >= 0.01
    x = rand(n,1);
    y = rand(n,1);
    p = 0;
    for i = 1:n
        if x(i)^2 + y(i)^2 <= 1
            p = p + 1;
        end
    end
    pi_hat(n) = 4*p/n;
    dif = abs(pi_hat(n) - pi_hat(n - 100));
    n = n + 100; % these part is the same as question 2
    pi = pi_hat(n - 100) % reture the estimate value of pi
    x1 = ones(n,1) - 2;  %define two new arrays to classify the points
    y1 = ones(n,1) - 2;
    x2 = ones(n,1) - 2;
    y2 = ones(n,1) - 2;
    for i = 1:(n-1)
        if x(i)^2 + y(i)^2 <= 1
            x1(i) = x(i); % store all the points inside the circle to (x1,y1)
            y1(i) = y(i);
        else
            x2(i) = x(i);
            y2(i) = y(i); % store points outside the circle to (x2,y2)
        end
    end
    f1 = find(x1 > -2);
    f2 = find(x2 > -2); %  function find can find the number we need and
    %reture where they are to array f1 and f2
    s1 = size(f1);
    s2 = size(f2);        %function size can compute arrays' size
    n1 = ones(s1,1);
    n2 = ones(s1,1);
    n3 = ones(s2,1);
    n4 = ones(s2,1);
    for i = 1 : s1
        n1(i) = x(f1(i));
        n2(i) = y(f1(i)); % store points inside the circle in a new array
    end
    for i = 1 : s2
        n3(i) = x(f2(i));
        n4(i) = y(f2(i)); % store points outside the circle in a new array
    end
    plot(n1,n2,'ro');
    hold on
    plot(n3,n4,'bo');
    xlabel('x');
    ylabel('y');
    title(sprintf('estimate value of pi is %d'),pi_hat);
    drawnow
    hold off
end
end