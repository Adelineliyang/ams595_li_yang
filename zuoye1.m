m = 2000; % set the number of sample size m
time = ones(m,1);
precision = ones(m,1);
for n = 1:m  
    tic; % start counting time
    x = rand(n,1); 
    y = rand(n,1); % set n random x and y, 0<=x<=1,0<=y<=1 
    p = 0; 
    for i = 1:n 
        if x(i)^2 + y(i)^2 <= 1
            p = p + 1; % if (x,y) is in the circle, we count that piont 
        end
        pi_hat = 4*p/n; % calculate pi_hat
        precision(n) = pi - pi_hat;
        time(n) = toc; % count time of each circle 
    end
    subplot(1,2,1)% put two graphs in one figure
    plot(1:m,precision,'m')  
    xlabel('number of random points')
    ylabel('precision')
    title('difference between estimate value of pi and precision') 
    subplot(1,2,2)
    plot(precision,time,'b.') 
    xlabel('time') 
    ylabel('precision')
    title('precision vs time') 
end
